Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. mempool optimizations: Top of my list is the work-in-progress (https://www.reddit.com/r/btc/comments/fe11ax/we_are_the_people_behind_bitcoin_cash_node_ask_us/fjlp4p2/) that was referred to by Jonathan Toomim. But also a review of the suggestions made by Tom Zander and discussed at length in this post (https://www.reddit.com/r/btc/comments/ceae5w/how_to_properly_do_spam_protection_of_mempool/), with relative wide agreement on sensible policies. Disabling or indeed removal of CPFP as a feature should be considered as an possible performance optimization step.
2. able to handle long unconfirmed chains (1000+)
3. removal of any algorithms with complexity > O(n) from the software


Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. no currency split in May
2. many more transactions in mempool
3. network capacity (effective block size limits) always >> mempool
4. gently relaxing the limitations around acceptable ("standard") transactions: [relax] Limitations on standard transactions are both in terms of their size and certain features. Occasionally I hear developers complain that they are running into feature or size limits when they are experimenting with new smart contracts. https://twitter.com/TobiasRuck/status/1191458689657573382 , https://twitter.com/TobiasRuck/status/1191455595846684672 . Not sure if the recent Flipstarter transactions are also impeded by standardness rules, but there definitely seems a low-ish limit on the number of participants in a single fundraiser, which could be due to such things. I wish BCHN would do a review of all limits related to standardness and consensus on transactions, and see if there are any of these numerical constraints which users bump into and which could be loosened a bit or even removed entirely or guarded by other constraints (space usage) without too much hazard:
   - maximum number of signature operations per transaction
   - number of opcodes per script
   - maximum script element size
   - maximum number of elements in a script
   - maximum number of public keys per multisig
   - maximum transaction size


Section C - Information about yourself
--------------------------------------

You would describe yourself as:

- [x] - other - please specify:  ToTheMempoolGuy ┗ (°0°)┛ 
