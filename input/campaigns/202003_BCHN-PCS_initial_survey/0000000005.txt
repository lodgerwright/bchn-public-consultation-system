Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

It can be anything: technical improvement, organizational, directional...

1. Collaboration with other nodes to remain RPC API compatible.
2. Support for multiple block propagation protocols.
3. Continued transparancy and public reporting.
4. Public transparent finances.

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

Generally things that affect the whole protocol / currency / ecosystem,
and not BCHN specifically.

1. Ecosystem diversity - full node implementations encouraging competition by actions like proper documentation, respectful discussions, transparent development and sharing resources (experience, bug fixes, etc).
2. Dedication to scalability - even if the consensus remains to keep limits low, I want each node software to actively pursue with high priority the changes that will enable radically faster performance.
3. Adoption for payments - widespread acceptance of BCH as a means of payment, in particulary for the basic and everyday needs (food, shelter, etc).

Section C - Information about yourself
--------------------------------------
This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [x] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [ ] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [ ] - other - please specify:
